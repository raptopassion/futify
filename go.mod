module gitlab.com/frenchutouch/futify

go 1.16

replace github.com/nanu-c/qml-go => github.com/manland/qml-go v0.0.0-20220224211303-83edb7061659

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/librespot-org/librespot-golang v0.0.0-20200423180623-b19a2f10c856
	github.com/miekg/dns v1.1.46 // indirect
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	github.com/zmb3/spotify v1.3.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/tools v0.1.9 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
