# 1.2.0 - unreleased

- [i18n] Updated dutch translation thanks @Vistaus
- [i18n] Updated hungarian translation thanks @Lundrin
- [i18n] Add german translation thanks @Schmuel

# 1.1.0 - released 01/03/2022

- [feature] Add a repeat button on the Player view thanks @discoverti
- [feature] Add a contributors section in about page
- [feature] Add translations mecanism
- [i18n] Add french translation
- [i18n] Add hungarian translation thanks @Lundrin
- [i18n] Add dutch translation thanks @Vistaus
- [bug] Avoid duplicate song in "Recently Play" thanks @discoverti
- [bug] Start album and playlist directly in search result (right action)

# 1.0.1 - released 17/02/2022

- [bug] Launch album in home crash app

# 1.0.0 - released 17/02/2022

- First release in open-store.
