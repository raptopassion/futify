Thanks to all this awesome people!

|                                                   Avatar                                                   |    Name     |                                                            Link                                                             |
| :--------------------------------------------------------------------------------------------------------: | :---------: | :-------------------------------------------------------------------------------------------------------------------------: |
| ![discoverti avatar](https://secure.gravatar.com/avatar/545bbc38f1ec3abf105d3a6f59e40375?s=90&d=identicon) | @discoverti | [merge requests](https://gitlab.com/frenchutouch/futify/-/merge_requests?scope=all&state=merged&author_username=discoverti) |
|  ![Lundrin avatar](https://secure.gravatar.com/avatar/6c1a30c76caed2e32383b3f9a6fd7a4a?s=90&d=identicon)   |  @Lundrin   |  [merge requests](https://gitlab.com/frenchutouch/futify/-/merge_requests?scope=all&state=merged&author_username=Lundrin)   |
|       ![Vistaus avatar](https://gitlab.com/uploads/-/system/user/avatar/2524611/avatar.png?width=90)       |  @Vistaus   |  [merge requests](https://gitlab.com/frenchutouch/futify/-/merge_requests?scope=all&state=merged&author_username=Vistaus)   |
|  ![Schmuel avatar](https://secure.gravatar.com/avatar/85fbb6fecf87fdccaec8a4bbaf926745?s=90&d=identicon)   |  @Schmuel   |  [merge requests](https://gitlab.com/frenchutouch/futify/-/merge_requests?scope=all&state=merged&author_username=Schmuel)   |
