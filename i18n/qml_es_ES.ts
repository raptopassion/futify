<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="29"/>
        <source>App:</source>
        <translation>App:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="36"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation>Futify es un cliente de spotify nativo no oficial</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>issues</source>
        <translation>Incidencias</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="61"/>
        <source>Author:</source>
        <translation>Autoría:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="94"/>
        <source>Contributors:</source>
        <translation>Colaboración:</translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="66"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="58"/>
        <source>popularity</source>
        <translation>popularidad</translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="74"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="89"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="94"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="122"/>
        <source>Your playlists</source>
        <translation>Tus listas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="144"/>
        <source>Your albums</source>
        <translation>Tus álbumes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="166"/>
        <source>Recently Play</source>
        <translation>Recientes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="182"/>
        <source>Featured playlist</source>
        <translation>Lista destacada</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="204"/>
        <source>Top tracks</source>
        <translation>Mejores pistas</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation>Nombre en spotify</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation>Iniciar sesión</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation>spotify.com</translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="72"/>
        <source>No song</source>
        <translation>No hay canción</translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="82"/>
        <source>followers</source>
        <translation>seguidores/as</translation>
    </message>
</context>
<context>
    <name>Playlists</name>
    <message>
        <location filename="../qml/components/Playlists.qml" line="66"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="30"/>
        <source>Queue</source>
        <translation>Lista de espera</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="34"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="42"/>
        <source>Play/Pause</source>
        <translation>Reproducir/Pausa</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>Precedent</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="62"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="71"/>
        <source>Repeat</source>
        <translation>Repetir</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="35"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="51"/>
        <source>Tracks</source>
        <translation>Pistas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="60"/>
        <source>Albums</source>
        <translation>Álbumes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="69"/>
        <source>Playlists</source>
        <translation>Listas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="78"/>
        <source>Artists</source>
        <translation>Artistas</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="41"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>Theme:</source>
        <translation>Tema:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>Dark</source>
        <translation>Oscuro</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>Light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="149"/>
        <source>Quality:</source>
        <translation>Calidad:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>low: prefer data over quality</source>
        <translation>baja: minimizar consumo de datos a costa de la calidad</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>medium</source>
        <translation>media</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>high: prefer quality over data</source>
        <translation>alta: maximizar calidad a costa del consumo de datos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="208"/>
        <source>Error song:</source>
        <translation>Canciones con error:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>silence: 3 seconds</source>
        <translation>silencio: 3 segundos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>error: usefull to know what happens</source>
        <translation>error: útil para saber qué ocurre</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="266"/>
        <source>Account:</source>
        <translation>Cuenta:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="275"/>
        <source>Log out</source>
        <translation>Cerrar sesión</translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation>popularidad</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation>Fin de cola de reproducción</translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation>pistas reproducidas hoy</translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation>No se ha reproducido nada</translation>
    </message>
</context>
</TS>
