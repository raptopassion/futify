import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12

Page {
    signal login(var username, var password)
    property string error: ""

    header: PageHeader {
        id: pageHeader
        title: "Futify"
        StyleHints {
            dividerColor: UbuntuColors.green
        }
    }

    Column {
        id: column

        anchors {
            top: pageHeader.bottom
            margins: units.gu(1)
            centerIn: parent
        }

        spacing: units.gu(5)

        Column {

            spacing: units.gu(1)

            TextField {
                id: username
                placeholderText: qsTr("Username")
                inputMethodHints: Qt.ImhEmailCharactersOnly
            }

            TextField {
                id: password
                placeholderText: qsTr("Password")
                echoMode: TextInput.Password
            }

            Label {
                visible: error != ""
                text: error
                color: "red"
            }

        }

        Button {
            text: qsTr('Log in')
            color: UbuntuColors.green
            anchors.right: parent.right
            onClicked: {
                login(username.text, password.text)
            }
        }
    }

    Row {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        bottomPadding: 30
        Button {
            text: qsTr('spotify.com')
            onClicked: {
                Qt.openUrlExternally('https://spotify.com')
            }
        }
    }
}